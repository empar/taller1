package dam.android.fabricio.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;
    private TextView tvDisplay;
    private Button buttonIncrease, buttonDecrease, buttonReset,buttonIncreasex2,buttonDecreasex2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUi();

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //Ex5  Cogemos el text del resultado y le pasamos el mensaje guardado
        TextView resultText = findViewById(R.id.tvDisplay);
        resultText.setText(savedInstanceState.getString("result"));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //Ex5 guardamos el mensaje del error
        outState.putString("result", tvDisplay.getText().toString());
    }


    private void setUi() {

        tvDisplay = findViewById(R.id.tvDisplay);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonReset = findViewById(R.id.buttonReset);

        //ex5 nuevos botones para incrementar o descrementar x2
        buttonDecreasex2 = findViewById(R.id.buttonDecreasex2);
        buttonIncreasex2 = findViewById(R.id.buttonIncreasex2);

        //set onclicklistener
        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
        buttonDecreasex2.setOnClickListener(this);
        buttonIncreasex2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonIncrease: count++;
                break;
            case R.id.buttonDecrease: count--;
                break;
            case R.id.buttonReset:
                count = 0;
                break;
            case R.id.buttonIncreasex2: count=count+2;
                break;
            case R.id.buttonDecreasex2: count=count-2;
                break;
        }
        tvDisplay.setText(getString(R.string.number_of_elements) + ": "+ count);
    }
}